ARG apline_version="3.18.3"

FROM alpine:${alpine_version}

ARG git_commit="unknown"
ARG build_date="unknown"

LABEL org.opencontainers.artifact.description="Duke University Libraries OCI helpers image"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/devops/oci-helpers"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.license="Apache-2.0"
LABEL org.opencontainers.image.created="${build_date}"
LABEL org.opencontainers.image.revision="${git_commit}"
LABEL org.opencontainers.image.authors="Duke University Libraries (https://library.duke.edu)"

RUN apk add --no-cache \
    buildah \
    crane \
    helm \
    podman \
    skopeo \
    && adduser -S builder

USER builder
